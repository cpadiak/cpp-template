# cpp-template

## Table of Contents

[[_TOC_]]

## Using in Your Project

### Dependencies

```
# for Arch
yay -S mold cmake ninja clang
```

### CMake Setup

Open `CMakeLists.txt` and:
* Replace all instances of `ReplaceMe` with your project name
* Uncomment all lines related to any packages you need

## Tooling Choices

This template uses the tooling that I like for my C++ projects. It is subject to change.

### Compilation & Linking

* Clang
* Mold

### Dynamic Analysis

* Address Sanitizer built-in to Clang
* TODO: profiling
* TODO: fuzzing

### Static Analysis

* Clang-Tidy

### Documentation

* TODO

### Testing

* doctest
* TODO: CI/CD
