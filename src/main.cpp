#include "main.hpp"

void print_usage(const clipp::group &cli, const std::string &programName) {
    const clipp::man_page &manPage = clipp::make_man_page(cli, programName, clipp::doc_formatting());
    for (const auto &page: manPage) {
        fmt::print("=== {} ===\n{}\n", page.title(), page.content());
    }
}

int main(int argc, char** argv) {
    spdlog::set_level(spdlog::level::warn);

    std::string configFilepath = "config.toml";
    std::string scriptFilepath = "dahnook.lua";

    auto cli = (
            clipp::option("--config") & clipp::value("configuration filepath", configFilepath),
                    clipp::option("--script") & clipp::value("script filepath", scriptFilepath)
    );

    if (!clipp::parse(argc, argv, cli)) {
        print_usage(cli, argv[0]);
        return 0;
    }

    if (configFilepath.empty()) {
        spdlog::error("specify a configuration filepath");
        print_usage(cli, argv[0]);
        return 0;
    }

    toml::table config;
    try {
        config = toml::parse_file(configFilepath);
    } catch (const toml::parse_error &err) {
        spdlog::error("could not find configuration file {}", configFilepath);
        return 1;
    }

    const auto configuredLevel = config["general"]["logging"].value<std::string>().value();
    const auto level = magic_enum::enum_cast<spdlog::level::level_enum>(configuredLevel).value();
    spdlog::set_level(level);

    return 0;
}
