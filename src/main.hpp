#include <assert.hpp>
#include <fmt/core.h>
#include <spdlog/spdlog.h>
#include <clipp.h>
#include <magic_enum.hpp>
#include <string>
#include <string_view>

#define SOL_ALL_SAFETIES_ON 1
#include <sol/sol.hpp>

#define TOML_HEADER_ONLY 0
#define TOML_IMPLEMENTATION
#include <toml++/toml.h>

#include <nlohmann/json.hpp>
using json = nlohmann::json;

#pragma once
